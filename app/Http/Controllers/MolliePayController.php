<?php


/**
 * File name: RazorPayController.php
 * Last modified: 2020.06.13 at 12:38:51
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2020
 *
 */

namespace App\Http\Controllers;

use App\Models\DeliveryAddress;
use App\Models\Payment;
use App\Models\User;
use App\Repositories\DeliveryAddressRepository;
use Illuminate\Http\Request;
use Flash;

class MolliePayController extends ParentOrderController
{
    private $api;
    private $currency;
    private $deliveryAddressRepo;

    public function __init()
    {
        $this->api = new \Mollie\Api\MollieApiClient();
        $this->api->setApiKey(config('services.mollie.key'));;
        $this->currency = setting('default_currency_code', 'INR');
        $this->deliveryAddressRepo = new DeliveryAddressRepository(app());
    }


    public function index()
    {
        return view('welcome');
    }


    public function checkout(Request $request)
    {
        try{
            $user = $this->userRepository->findByField('api_token', $request->get('api_token'))->first();
            $delivery_id = $request->get('delivery_address_id');
//            $deliveryAddress = $this->deliveryAddressRepo->findWithoutFail($delivery_id);
            if (!empty($user)) {
                $this->order->user = $user;
                $this->order->user_id = $user->id;
                $this->order->delivery_address_id = $delivery_id;
                $molliePayCart = $this->getOrderData();
                $molliePayOrder = $this->api->payments->create($molliePayCart);
                if (!empty($molliePayOrder->getCheckoutUrl())) {
                    return redirect($molliePayOrder->getCheckoutUrl());
                } else {
                    session()->put(['code' => 'danger', 'message' => "Error processing Mollie payment for your order"]);
                    return redirect(route('payments.failed'));
                }
            }else{
                Flash::error("Error processing RazorPay user not found");
                return redirect(route('payments.failed'));
            }
        }catch (\Exception $e){
            Flash::error("Error processing RazorPay payment for your order :" . $e->getMessage());
            return redirect(route('payments.failed'));
        }
    }


    /**
     * @param int $userId
     * @param int $deliveryAddressId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function paySuccess(Request $request)
    {
        $data = $request->all();
        $payment = $this->api->payments->get($data["id"]);
        $orderId = $payment->metadata->order_id;
        $userId = $payment->metadata->user_id;
        $deliveryAddressId = $payment->metadata->delivery_address_id;

        if ($payment->isPaid() && !$payment->hasRefunds() && !$payment->hasChargebacks()) {
            /*
             * The payment is paid and isn't refunded or charged back.
             * At this point you'd probably want to start the process of delivering the product to the customer.
             */
            $description = $this->getPaymentDescription($data);

            $this->order->user_id = $userId;
            $this->order->user = $this->userRepository->findWithoutFail($userId);
            $this->order->delivery_address_id = $deliveryAddressId;


            if ($request->hasAny(['id'])) {

                $this->order->payment = new Payment();
                $this->order->payment->status = trans('lang.order_paid');
                $this->order->payment->method = 'MolliePay';
                $this->order->payment->description = $description;
                $this->createOrder();
                if (!empty($this->order)) {
                    session()->put(['code' => 'success', 'message' => "Order {$this->order->id} has been paid successfully!"]);
                } else {
                    session()->put(['code' => 'danger', 'message' => "Error processing Mollie payment for Order!"]);

                }
            }else{
                Flash::error("Error processing Mollie payment for your order");
            }
        } elseif ($payment->isOpen()) {
            /*
             * The payment is open.
             */
        } elseif ($payment->isPending()) {
            /*
             * The payment is pending.
             */
        } elseif ($payment->isFailed()) {
            /*
             * The payment has failed.
             */
        } elseif ($payment->isExpired()) {
            /*
             * The payment is expired.
             */
        } elseif ($payment->isCanceled()) {
            /*
             * The payment has been canceled.
             */
        } elseif ($payment->hasRefunds()) {
            /*
             * The payment has been (partially) refunded.
             * The status of the payment is still "paid"
             */
        } elseif ($payment->hasChargebacks()) {
            /*
             * The payment has been (partially) charged back.
             * The status of the payment is still "paid"
             */
        }
    }

    /**
     * Set cart data for processing payment on PayPal.
     *
     *
     * @return array
     */
    private function getOrderData()
    {
        $data = [];
        $this->calculateTotal();
        $amountINR = $this->total;
        /*if ($this->currency !== 'INR') {
            $url = "https://api.exchangeratesapi.io/latest?symbols=$this->currency&base=INR";
            $exchange = json_decode(file_get_contents($url), true);
            $amountINR =  $this->total / $exchange['rates'][$this->currency];
        }*/
        $orderId = $this->paymentRepository->all()->count() + 1;
        $data['amount'] = [
            "currency" => $this->currency,
            "value" => $amountINR
        ];
        $data['description'] = "Order #{$orderId}";
        $data['redirectUrl'] = route(url('payments/mollie'));
        $data['webhookUrl'] = route(url('payments/mollie/webhook'));
        $data['metadata'] = [
            "order_id" => $orderId,
            "user_id" => $this->order->user_id,
            "delivery_address_id" => $this->order->delivery_address_id,
        ];

        return $data;
    }

    /**
     * @param $razorPayOrder
     * @param User $user
     * @param DeliveryAddress $deliveryAddress
     * @return array
     */
    /*private function getRazorPayFields($razorPayOrder, User $user, DeliveryAddress $deliveryAddress): array
    {
        $market = $this->order->user->cart[0]->product->market;

        $fields = array(
            'key_id' => config('services.razorpay.key', ''),
            'order_id' => $razorPayOrder['id'],
            'name' => $market->name,
            'description' => count($this->order->user->cart) ." items",
            'image' => $this->order->user->cart[0]->product->market->getFirstMedia('image')->getUrl('thumb'),
            'prefill' => [
                'name' => $user->name,
                'email' => $user->email,
                'contact' => $user->custom_fields['phone']['value'],
            ],
            'callback_url' => url('payments/razorpay/pay-success',['user_id'=>$user->id,'delivery_address_id'=>$deliveryAddress->id]),

        );

        if (!empty($deliveryAddress)) {
            $fields ['notes'] = [
                'delivery_address' => $deliveryAddress->address,
            ];
        }


        if ($this->currency !== 'INR') {
            $fields['display_amount'] = $this->total;
            $fields['display_currency'] = $this->currency;
        }
        return $fields;
    }*/

    /**
     * @param array $data
     * @return string
     */
    private function getPaymentDescription(array $data): string
    {
        $description = "Id: " . $data['id'] . "</br>";
        $description .= trans('lang.order').": " . $data['orderNumber'];
        return $description;
    }

}
