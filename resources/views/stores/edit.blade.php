@extends('layouts.app')
@push('css_lib')
<!-- iCheck -->
<link rel="stylesheet" href="{{asset('plugins/iCheck/flat/blue.css')}}">
<!-- select2 -->
<link rel="stylesheet" href="{{asset('plugins/select2/select2.min.css')}}">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="{{asset('plugins/summernote/summernote-bs4.css')}}">
{{--dropzone--}}
<link rel="stylesheet" href="{{asset('plugins/dropzone/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
@endpush
@section('content')
  @php
    $i = 1
  @endphp
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">{{trans('lang.store_plural')}} <small>{{trans('lang.store_desc')}}</small></h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{url('/dashboard')}}"><i class="fa fa-dashboard"></i> {{trans('lang.dashboard')}}</a></li>
          <li class="breadcrumb-item"><a href="{!! route('stores.index') !!}">{{trans('lang.store_plural')}}</a>
          </li>
          <li class="breadcrumb-item active">{{trans('lang.store_edit')}}</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<div class="content">
  <div class="clearfix"></div>
  @include('flash::message')
  @include('adminlte-templates::common.errors')
  <div class="clearfix"></div>
  <div class="card">
    <div class="card-header">
      <ul class="nav nav-tabs align-items-end card-header-tabs w-100">
        @can('stores.index')
        <li class="nav-item">
          <a class="nav-link" href="{!! route('stores.index') !!}"><i class="fa fa-list mr-2"></i>{{trans('lang.store_table')}}</a>
        </li>
        @endcan
        @can('stores.create')
        <li class="nav-item">
          <a class="nav-link" href="{!! route('stores.create') !!}"><i class="fa fa-plus mr-2"></i>{{trans('lang.store_create')}}</a>
        </li>
        @endcan
        <li class="nav-item">
          <a class="nav-link active" href="{!! url()->current() !!}"><i class="fa fa-pencil mr-2"></i>{{trans('lang.store_edit')}}</a>
        </li>
      </ul>
    </div>
    <div class="card-body">
      {!! Form::model($store, ['route' => ['stores.update', $store->id], 'method' => 'patch']) !!}
      <div class="row">
        @include('stores.fields')
      </div>
      {!! Form::close() !!}
      <div class="clearfix"></div>
    </div>
  </div>
</div>
@include('layouts.media_modal')
@endsection
@push('scripts_lib')
<!-- iCheck -->
<script src="{{asset('plugins/iCheck/icheck.min.js')}}"></script>
<!-- select2 -->
<script src="{{asset('plugins/select2/select2.min.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('plugins/summernote/summernote-bs4.min.js')}}"></script>
{{--dropzone--}}
<script src="{{asset('plugins/dropzone/dropzone.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js" integrity="sha512-rmZcZsyhe0/MAjquhTgiUcb4d9knaFc7b5xAfju483gbEXTkeJRUMIPk6s3ySZMYUHEcjKbjLjyddGWMrNEvZg==" crossorigin="anonymous"></script>
<script src="{{asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<script type="text/javascript">
  //Timepicker
  $('.timepicker').datetimepicker({
    format: 'LT'
  })
  function addData(a) {
    var from_main = $("#from_main input[type='text']").val();
    var to_main = $("#to_main input[type='text']").val();
    var html = '<tr id="row_' + i + '">\n' +
            '<td>\n' +
            '<input type="hidden" name="slots[' + i + '][from]" value="'+from_main+'"><label class="control-label">'+from_main+'</lable></td><td>\n' +
            '<input type="hidden" name="slots[' + i + '][to]" value="'+to_main+'"><label class="control-label">'+to_main+'</lable>\n' +
            '</td><td>\n' +
            '<input type="button" name="remove" value="Remove" onClick="removeData(this,' + i + ');"  class="btn btn-primary" />\n' +
            '</td></tr>';

    $("#changeData").append(html);
    $("#from_main input[type='text']").val('');
    $("#to_main input[type='text']").val('');
    i++
  }

  function removeData(a, t) {
    $("#row_" + t).remove();
  }
  $.fn.datetimepicker.Constructor.Default = $.extend({}, $.fn.datetimepicker.Constructor.Default, {
    format: 'LT'
     });
    Dropzone.autoDiscover = false;
    var dropzoneFields = [];
</script>
@endpush
